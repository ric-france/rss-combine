import * as _ from 'lodash';
import * as FeedParser from 'feedparser';
import fetch from 'node-fetch';
import * as iconv from 'iconv-lite';
import * as RSS from 'rss';
import * as htmlspecialchars from 'htmlspecialchars';
import * as Cache from 'stale-lru-cache';
import * as probe from 'probe-image-size';
const truncate = require('truncate-html');

const cache = new Cache({
  maxSize: 1000,
  maxAge: 300,
  staleWhileRevalidate: 3600,
  // revalidate: function (key, callback) {
  //   fetchSomeAsyncData(callback);
  // },
});

const cacheImageSize = new Cache({
  maxSize: 10000,
  maxAge: 864000,
  staleWhileRevalidate: 86400,
  // revalidate: function (key, callback) {
  //   fetchSomeAsyncData(callback);
  // },
});

const omit = (keysToOmit: string[], originalObj = {}) =>
  Object.fromEntries(
    Object.entries(originalObj).filter(([key]) => !keysToOmit.includes(key))
  );

export default class RSSCombine {
  constructor() {}

  async get(feed: string) {
    const items = [];
    let meta;

    // Setup feedParser stream
    var feedParser = new FeedParser({
      normalize: true,
    });
    feedParser.on('error', (error) => {
      throw new Error(error);
    });

    feedParser.on('readable', function () {
      var item;
      meta = this.meta;

      while ((item = this.read())) {
        items.push(item);
      }
    });

    // Get a response stream
    const res = await fetch(feed, {
      headers: {
        'user-agent': 'rss-combine/1.0  (+https://www.ric-france.fr/)',
        accept: 'text/html,application/rss+xml,application/xhtml+xml',
      },
      compress: true,
    }).catch(this.errorHandler);

    if (!res) throw new Error('No response');

    // Handle our response and pipe it to feedParser
    if (res.status != 200) throw new Error(`Bad status code: ${res.status}`);

    // console.debug('responseHeaders', res.headers);

    var charset = this.getParams(res.headers.get('content-type') || '').charset;

    var responseStream = res.body;

    responseStream = this.maybeTranslate(responseStream, charset);

    // And boom goes the dynamite
    responseStream.pipe(feedParser);

    if (res.redirected) {
      console.warn(`Redirected <${feed}> to: ${res.url}`);
    }

    return await new Promise((resolve, reject) => {
      responseStream.on('end', () => {
        resolve({
          headers: res.headers,
          items,
          meta,
          redirected: res.redirected,
        });
      });
    });
  }

  async cachedGet(feed: string) {
    return await new Promise((resolve, reject) => {
      cache.wrap(
        feed,
        (key, cb) => {
          this.get(key)
            .then((value: any) => {
              // if could not fetch feed, don't cache
              if (!value) {
                cb('failed to fetch stream');
              }

              let cacheControl =
                value.headers.get('cache-control') ||
                (value.meta.ttl
                  ? `max-age=${value.meta.ttl * 60}`
                  : 'max-age=60'); // fallback 5min cache

              cacheControl = cacheControl
                .split(/\,\s*/g)
                // we only keep max-age
                .filter((key) => /^max-age\=[^\,]+/.test(key))
                // .concat('stale-while-revalidate=86400')
                .join(', ');

              console.debug(`Cached <${key}>: ${cacheControl}`);
              cb(null, value, cacheControl);
            })

            .catch((error) => {
              console.warn('Failed to fetch %s', key);
              cb(error);
            });
        },
        (error, value) => {
          if (error) reject(error);

          resolve(value);
        }
      );
    });
  }

  private errorHandler(error) {
    console.error(error);
  }

  private maybeTranslate(res, charset) {
    var iconvStream;
    // Decode using iconv-lite if its not utf8 already.
    if (!iconvStream && charset && !/utf-?8/i.test(charset)) {
      try {
        iconvStream = iconv.decodeStream(charset);
        console.debug('Converting from charset %s to utf-8', charset);
        iconvStream.on('error', this.errorHandler);
        // If we're using iconvStream, stream will be the output of iconvStream
        // otherwise it will remain the output of request
        res = res.pipe(iconvStream);
      } catch (err) {
        res.emit('error', err);
      }
    }
    return res;
  }

  private getParams(str) {
    var params = str.split(';').reduce(function (params, param) {
      var parts = param.split('=').map(function (part) {
        return part.trim();
      });
      if (parts.length === 2) {
        params[parts[0]] = parts[1];
      }
      return params;
    }, {});
    return params;
  }

  public async combine(feedConfig): Promise<any> {
    if (
      !feedConfig.feeds ||
      feedConfig.feeds.length === 0 ||
      !feedConfig.size
    ) {
      throw new Error('Feeds and size are required feedConfig values');
    }

    feedConfig.generator = feedConfig.generator || 'rss-combine for Node';

    feedConfig.link = feedConfig.link || 'https://convergence.ric-france.fr/';

    // Strip properties 'feeds' and 'size' from config to be passed to `rss` module
    const strippedConfig = omit(['feeds', 'size'], feedConfig);

    const feeds = [];

    for (const feed of feedConfig.feeds) {
      feeds.push(this.cachedGet(feed));
    }

    return await new Promise((resolve, reject) => {
      Promise.all(feeds)
        .catch((e) => {
          // reject(e);
        })
        .then((feeds) => (feeds || []).map((feed) => feed.items))
        .then(_.flatten)
        .then((entries) => {
          return _.sortBy(entries, this.sortEntries);
        })

        // filter out duplicate urls or title
        .then((entries) =>
          // inspired by https://stackoverflow.com/questions/2218999/how-to-remove-all-duplicates-from-an-array-of-objects
          // to keep last occurrence
          // arr.slice().reverse().filter((v,i,a)=>a.findIndex(t=>(t.id === v.id))===i).reverse()
          entries.filter(
            (v, i, a) =>
              a.findIndex((t) => t.link == v.link || t.title == v.title) === i
          )
        )
        .then((entries) => {
          return _.take(entries, feedConfig.size);
        })
        .then(async (entries) => {
          // truncate.setup({
          //   byWords: true,
          //   // stripTags: false,
          //   ellipsis: '…',
          //   // decodeEntities: false,
          //   // keepWhitespaces: false,
          //   // excludes: '',
          //   reserveLastWord: true,
          //   stop: 20,
          // });

          for (let entry of entries) {
            entry.url = entry.link;

            // WP crap: <p>The post <a rel="nofollow" href="https://label.ric-france.fr/evaluations/analyse-du-ric-de-florian-philippot-les-patriotes" data-wpel-link="internal">Analyse du RIC de Florian Philippot &#8211; Les Patriotes</a> appeared first on <a rel="nofollow" href="https://label.ric-france.fr" data-wpel-link="internal">Label RIC</a>.</p>
            if (/^<p>The post <a rel="nofollow"/.test(entry.description))
              entry.description = null;

            entry.description = entry.content || entry.description || '';

            // youtube media
            if (/^yt:/.test(entry.guid || '')) {
              if (!/vid[eé]o/i.test(entry.title))
                entry.title = `[Vidéo] ` + entry.title;

              // entry.description =
              //   entry.description ||
              //   entry['media:group']['media:description']['#'];

              if (entry['media:group']['media:thumbnail']) {
                // console.log(entry['media:group']['media:thumbnail']);
                entry.enclosures = entry.enclosures || [];

                entry.enclosures.push(
                  entry['media:group']['media:thumbnail']['@']
                );
              }
            }

            // get a short version of the description
            // entry.description = truncate(entry.description, 200, {
            //   byWords: true,
            //   ellipsis: '…',
            // });

            // entry.description =
            //   entry.summary +
            //     (entry.summary != entry.description
            //       ? `\n<!-- truncated by rss-combine -->`
            //       : '') || entry.description;

            // console.log(entry);
            entry.description = truncate(
              entry.description ||
                ''
                  .replace(/<p[^>]*>\s*<\/p>/gi, '')
                  .replace(/<div[^>]*>\s*<\/div>/gi, '')
                  .replace(/<h2[^>]*>.*?<\/h2>/gi, '')
                  .replace(/<h3[^>]*>.*?<\/h3>/gi, '')
                  .replace(/<h4[^>]*>.*?<\/h4>/gi, '')
                  .replace(/<h5[^>]*>.*?<\/h5>/gi, '')
                  .replace(/<h6[^>]*>.*?<\/h6>/gi, '')
                  .replace(/<iframe[^>]*>.*?<\/iframe>/gi, '')
                  .replace(/<figure[^>]*>.*?<img.*?<\/figure>/gi, '')
                  .replace(/<a[^>]*>.*?<img.*?<\/a>/gi, '')
                  .replace(/<img[^>]*>/gi, '')
                  // remove view as pdf on chouard.org
                  .replace(/<div[^>]*>.*?Page en PDF.*?<\/div>/gi, ''),
              80,
              {
                byWords: true,

                ellipsis: ` … <a href="${entry.url}" title="${htmlspecialchars(
                  entry.title
                )}">Lire la suite</a>`,
                reserveLastWord: 50,
              }
            );

            // if there is an enclosed image
            if (
              entry.enclosures &&
              entry.enclosures.length
              // &&
              // /^image/.test(entry.enclosures[0].type)
            ) {
              entry.enclosure = entry.enclosures[0];

              if (entry.enclosure.type && entry.enclosure.type == 'image') {
                delete entry.enclosure.type;
              }

              // entry.enclosure.size = entry.enclosure.length
              //   ? entry.enclosure.length
              //   : 1234;

              if (
                !entry.enclosure.length ||
                !entry.enclosure.width ||
                !entry.enclosure.height
              ) {
                // const enclosureProbe = await probe(entry.enclosure.url, {});

                const imageSize = await this.getImageSize(entry.enclosure.url);

                if (imageSize) {
                  entry.enclosure.length = imageSize.length;
                  // entry.enclosure.type = imageSize.type; this return just 'jpg', not mime type (the rss parser adds it properly)
                  entry.enclosure.url = imageSize.url; // the final redirected url
                  entry.enclosure.width = imageSize.width;
                  entry.enclosure.height = imageSize.height;
                }
              }

              entry.enclosure.size = entry.enclosure.length;
              // and that the description has no image
              if (!/<img/.test(entry.description)) {
                // we add a linked image
                entry.description = `<!-- added by rss-combine -->
<figure class="rss-combine"><a href="${htmlspecialchars(entry.link)}"
                style="display:block; text-align:center;"><img src="${htmlspecialchars(
                  entry.enclosure.url
                )}" alt="${htmlspecialchars(entry.title)}" width="${
                  entry.enclosure.width
                }" height="${entry.enclosure.height}"
                style="display:block; margin-left:auto; margin-right:auto;" /></a></figure>
<!-- /added by rss-combine -->
${entry.description}`;
              }
            }
          }

          return entries;
        })

        .then((entries) => {
          return this.createFeed(strippedConfig, entries);
        })

        .then((createdFeed) => {
          resolve(createdFeed);
        });
    });
  }

  async getImageSize(url): Promise<probe.ProbeResult> | null {
    if (!url) return null;

    let imageSize;

    try {
      imageSize = await new Promise<probe.ProbeResult>(
        (resolve: any, reject): any => {
          cacheImageSize.wrap(
            url,
            (key, cb) => {
              // console.debug(`getting imageSize of: <${key}>`);

              try {
                probe(encodeURI(key), {})
                  .catch((error) => cb(error))
                  .then((value) => {
                    if (!value) cb('Failed to get image size <${key}>');

                    cb(null, value, {});
                  });
              } catch (error) {
                cb(error);
              }
            },
            (error, imageSize) => {
              if (error) reject(error);

              // console.debug(`got imageSize of: <${imageSize}>`);

              resolve(imageSize);
            }
          );
        }
      );
    } catch (error) {
      console.error(error);
    }

    return imageSize || null;
  }

  private sortEntries(entry) {
    if (entry) {
      var date = new Date(entry.pubdate);
      var time = date.getTime();
      return time * -1;
    } else {
      return null;
    }
  }

  private createFeed(feedConfig, entries): any {
    const newFeed = new RSS(feedConfig);

    for (let entry of entries) {
      newFeed.item(entry);
    }
    return newFeed;
  }
}
