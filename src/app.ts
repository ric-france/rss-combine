import * as express from 'express';
import * as Cache from 'stale-lru-cache';
import RSS from 'rss';
import RSSCombine from './helpers/rss-combine';

const PORT = process.env.PORT || 3000;

const app = express();

const cache = new Cache({
  maxSize: 1000,
  maxAge: 10,
  staleWhileRevalidate: 600,
  // revalidate: function (key, callback) {
  //   fetchSomeAsyncData(callback);
  // },
});

const rssCombine = new RSSCombine();

let feedConfig = {
  title: `L'actu du RIC`,
  size: 100,
  feeds: [
    'https://www.clic-ric.org/feed',
    'https://convergence.ric-france.fr/feed',
    'https://www.espoir-ric.fr/feed',
    'https://label.ric-france.fr/feed',
    'https://www.ric-france.fr/feed',
    'https://www.article3.fr/feed',
    'https://www.mouvement-constituant-populaire.fr/feed',
    'https://culture-ric.fr/feed/',
    'https://www.chouard.org/feed',
    'https://www.youtube.com/feeds/videos.xml?channel_id=UC39EygSePQOHyRA6xu_NLLg', // Étienne Chouard
    'https://www.youtube.com/feeds/videos.xml?channel_id=UCWGsN59FON3vOtXCozQPsZQ', // Canard Réfractaire
    'https://www.youtube.com/feeds/videos.xml?channel_id=UC6acMk7qWLp7FHk7DepBsKg', // Objectif RIC
  ],
  feed_url: 'https://convergence.ric-france.fr/rss-combine/feed',
  pubDate: new Date(),
  softFail: true,
  site_url: 'https://convergence.ric-france.fr/',
};

app.get('/feed', async (req, res, next) => {
  // default 20 - min: 1 - max: 500
  let limit = req.query.limit || 20;
  feedConfig.size = Math.max(1, Math.min(500, limit));

  cache.wrap(
    { url: feedConfig.feed_url, limit },
    (key, cb) => {
      // revalidate
      rssCombine
        .combine(feedConfig)
        .then((value) => {
          cb(null, value.xml({ indent: true }), {});
        })
        .catch((error) => {
          cb(error);
        });
    },
    (error, value) => {
      if (error) throw new Error(error);

      res.setHeader('Content-Type', 'application/rss+xml; charset=utf-8');
      res.end(value);
    }
  );
});

app.listen(PORT, 'localhost', () => {
  console.log(`Application serving on http://localhost:${PORT}/`);
});
